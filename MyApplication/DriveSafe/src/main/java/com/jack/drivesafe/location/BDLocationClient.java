package com.jack.drivesafe.location;

import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.jack.drivesafe.MainActivity;
import com.jack.drivesafe.MyApplication;

public class BDLocationClient {

	private static LocationClient mLocationClient = null;

	private static MyLocationListener myListener = null;

	private static BDLocationClient bdLocationClient = null;

	private BDLocationClient() {
	}

	public static BDLocationClient getInstance() {
		if (bdLocationClient == null) {
			bdLocationClient = new BDLocationClient();
			myListener = new MyLocationListener();
			mLocationClient = new LocationClient(MyApplication.getContext());
			initBaiduLocation();
		}
		return bdLocationClient;
	}

	public void setReturnListener(MyLocationListener.ReturnLocationListener listener) {
		if (myListener != null) {
			myListener.setReturnListener(listener);
		}
	}

	private static void initBaiduLocation() {
		mLocationClient.registerLocationListener(myListener);
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);// 设置定位模式
		option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度，默认值gcj02
		option.setScanSpan(MainActivity.TIME);// 设置发起定位请求的间隔时间为5000ms 小与1000 则只定位一次
		option.setProdName("za");
		option.setIsNeedAddress(true);
		mLocationClient.setLocOption(option);
		mLocationClient.start();
	}

	public void stopLocation() {
		if (mLocationClient != null && mLocationClient.isStarted()) {
			mLocationClient.stop();
		}
	}

	/**
	 * 请求定位
	 */
	public void requestLocation() {
		if (mLocationClient != null && mLocationClient.isStarted()) {
			mLocationClient.requestLocation();
		} else {
			mLocationClient.start();
		}
	}

	public void unRegisterLocationListener() {
		if (myListener != null && mLocationClient != null) {
			mLocationClient.unRegisterLocationListener(myListener);
		}
	}

	public void stop() {
		if (mLocationClient != null) {
			mLocationClient.stop();
		}
	}
}
