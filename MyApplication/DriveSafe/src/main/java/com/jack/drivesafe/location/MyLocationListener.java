package com.jack.drivesafe.location;

import android.util.Log;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;

public class MyLocationListener implements BDLocationListener {

    private ReturnLocationListener mListener;

    public void setReturnListener(ReturnLocationListener listener) {
        mListener = listener;
    }

    @Override
    public void onReceiveLocation(final BDLocation location) {
        if (location == null) {
            return;
        }
        StringBuffer sb = new StringBuffer(256);
        sb.append("时间 : ");
        sb.append(location.getTime());
        sb.append("\n定位结果 : ");
        if (location.getLocType() == BDLocation.TypeGpsLocation) {
            sb.append("ok");
            sb.append("\n经度 : ");
            sb.append(location.getLongitude());
            sb.append("\n纬度 : ");
            sb.append(location.getLatitude());
            sb.append("\n方向 : ");
            sb.append(location.getDirection() + "°");
            sb.append("\n速度 : ");
            sb.append(location.getSpeed() + "km/h");
            sb.append("\n高度 : ");
            sb.append(location.getAltitude() + "米");
        } else {
            sb.append("定位中(" + location.getLocType() + ")... ...");
        }
        Log.e("test", sb.toString());
        if (location == null || location.getLatitude() == 4.9E-324
                || location.getLongitude() == 4.9E-324) {
            return;
        }
//		BDLocationClient.getInstance().stop();
        if (mListener != null) {
            mListener.success(location, sb.toString());
        }
    }

    /**
     * 成功返回Location对象后，回调通知Activity
     */
    public static interface ReturnLocationListener {
        /**
         * Location成功
         *
         * @param location
         * @param logs
         */
        public void success(BDLocation location, String logs);
    }

}