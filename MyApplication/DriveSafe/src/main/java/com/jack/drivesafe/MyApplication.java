package com.jack.drivesafe;

import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.text.TextUtils;

import com.jack.drivesafe.location.BDLocationClient;

/**
 * @description MyApplication
 */
public class MyApplication extends Application {

	private static Context appContext = null;

	private static String mHttpCookie;
	
	@Override
	public void onCreate() {
		super.onCreate();
		setContext();
		if (isMainProcessType()) {
			BDLocationClient.getInstance();
		}
	}

	/**
	 * 获得后台线程的优先级 默认是Thread.NORM_PRIORITY
	 * 
	 * @return 优先级@see #Thread.NORM_PRIORITY
	 */
	protected int getWorkLooperThreadPriority() {
		return Process.THREAD_PRIORITY_BACKGROUND;
	}

	public static Context getContext() {
		return appContext;
	}

	public static void sendBroadcast(String action) {
		if (appContext != null) {
			appContext.sendBroadcast(new Intent(action));
		}
	}

	private void setContext() {
		appContext = this.getApplicationContext();
	}

	private boolean isMainProcessType() {
		String processName = getCurrentProcessName();
		if (getPackageName().equals(processName)) {
			return true;
		} else {
			return false;
		}
	}

	private String getCurrentProcessName() {
		int pid = Process.myPid();
		ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> infos = am.getRunningAppProcesses();
		if (infos != null && infos.size() > 0) {
			for (RunningAppProcessInfo appProcess : infos) {
				if (appProcess.pid == pid) {
					return appProcess.processName;
				}
			}
		}
		return "";
	}

	public static String getmHttpCookie() {
		return mHttpCookie;
	}

	public static void setmHttpCookie(String mHttpCookie) {
		if(!TextUtils.isEmpty(mHttpCookie)){
			MyApplication.mHttpCookie = mHttpCookie;
		}
	}
	

}
