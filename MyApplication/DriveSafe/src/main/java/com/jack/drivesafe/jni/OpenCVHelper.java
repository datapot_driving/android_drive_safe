package com.jack.drivesafe.jni;

public class OpenCVHelper {

    static {
        System.loadLibrary("OpenCV");
    }

    public static native int[] gray(int[] buf, int w, int h);
}