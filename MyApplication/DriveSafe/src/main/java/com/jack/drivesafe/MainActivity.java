package com.jack.drivesafe;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.jack.drivesafe.jni.OpenCVHelper;
import com.jack.drivesafe.location.BDLocationClient;
import com.jack.drivesafe.location.MyLocationListener;

import java.util.Date;

public class MainActivity extends Activity {

    public static final int TIME = 2000;
    private TextView textView;
    private ImageView img;
    private SensorManager sm;
    private Date date;
    private int gpsResult;
    private double longitude;
    private double latitude;
    private float speed;
    private float direction;
    private double altitude;
    private String addressStr;
    private float x;
    private float y;
    private float z;
    private long lastRefreshUITime = System.currentTimeMillis();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.data);
        img = (ImageView) findViewById(R.id.img);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(
                        R.drawable.ic)).getBitmap();
                int w = bitmap.getWidth(), h = bitmap.getHeight();
                int[] pix = new int[w * h];
                bitmap.getPixels(pix, 0, w, 0, 0, w, h);
                int[] resultPixes = OpenCVHelper.gray(pix, w, h);
                Bitmap result = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
                result.setPixels(resultPixes, 0, w, 0, 0, w, h);
                img.setImageBitmap(result);
            }
        });
        //创建一个SensorManager来获取系统的传感器服务
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //选取加速度感应器
        int sensorType = Sensor.TYPE_ACCELEROMETER;
        sm.registerListener(myAccelerometerListener, sm.getDefaultSensor(sensorType), SensorManager.SENSOR_DELAY_NORMAL);
        BDLocationClient.getInstance().setReturnListener(new MyLocationListener.ReturnLocationListener() {
            @Override
            public void success(BDLocation location, final String logs) {
                if (location != null && (location.getLocType() == BDLocation.TypeGpsLocation || location.getLocType() == BDLocation.TypeNetWorkLocation)) {
                    gpsResult = location.getLocType();
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                    addressStr = location.getAddrStr();
                    if (location.getLocType() == BDLocation.TypeGpsLocation) {
                        speed = location.getSpeed();
                        altitude = location.getAltitude();
                        direction = location.getDirection();
                    } else {
                        speed = 0.0f;
                        altitude = 0.0f;
                        direction = 0.0f;
                    }
                } else {
                    gpsResult = -1;
                }
                refreshUI();
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /*
    * SensorEventListener接口的实现，需要实现两个方法
    * 方法1 onSensorChanged 当数据变化的时候被触发调用
    * 方法2 onAccuracyChanged 当获得数据的精度发生变化的时候被调用，比如突然无法获得数据时
    * */
    final SensorEventListener myAccelerometerListener = new SensorEventListener() {

        //复写onSensorChanged方法
        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                //图解中已经解释三个值的含义
                x = sensorEvent.values[0];
                y = sensorEvent.values[1];
                z = sensorEvent.values[2];
                refreshUI();
            }
        }

        //复写onAccuracyChanged方法
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    private void refreshUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (System.currentTimeMillis() - lastRefreshUITime > 500) {
                    StringBuffer sb = new StringBuffer(256);
                    sb.append("时间 : " + new Date().toLocaleString());
                    sb.append("\n定位结果 : ");
                    if (gpsResult == BDLocation.TypeGpsLocation || gpsResult == BDLocation.TypeNetWorkLocation) {
                        if (gpsResult == BDLocation.TypeGpsLocation) {
                            sb.append("ok(GPS)");
                        } else if (gpsResult == BDLocation.TypeNetWorkLocation) {
                            sb.append("ok(网络)");
                        }
                        sb.append("\n经度 : ");
                        sb.append(longitude);
                        sb.append("\n纬度 : ");
                        sb.append(latitude);
                        sb.append("\n方向 : ");
                        sb.append(direction + "°");
                        sb.append("\n速度 : ");
                        sb.append(speed + "km/h");
                        sb.append("\n高度 : ");
                        sb.append(altitude + "米");
                        sb.append("\n地址 : ");
                        sb.append(addressStr);
                    } else {
                        sb.append("定位中(" + gpsResult + ")... ...");
                    }
                    sb.append("\n加速度x：");
                    sb.append(x);
                    sb.append("\n加速度y：");
                    sb.append(y);
                    sb.append("\n加速度z：");
                    sb.append(z);
                    textView.setText(sb.toString());
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sm.unregisterListener(myAccelerometerListener);
        BDLocationClient.getInstance().stop();
    }

}
